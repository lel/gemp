# gemp

Publish a file in the style of Michael Lazar's Gemini Markdown Proposal to a directory of a gemini site, and insert a formatted link to it in the index of that directory at a particular line.

The files are formatted using two files from [Michael Lazar's Gemini Markdown Proposal post](gemini://mozz.us/markdown/), extract_ast.py and render_vt100.py, which must be in $PATH and executable. (see gemini://mozz.us/markdown/ for more info)

For user convenience I have thrown these into the repo, and I don't see any licensing info anywhere in there so I hope mozz is okay with that.


## Installation

```bash
git clone https://git.envs.net/lel/gemp
cd gemp
ln -s $(pwd)/gemp ~/.local/bin
ln -s $(pwd)/*.py ~/.local/bin
```

Then edit gemp and change the SERVER and WHERE_TO variables to your ssh server and the dir on that server where you want to publish your blog.

That directory will need to already have an index.gmi file with LINE lines where LINE is defined at the beginning of gemp (default of 4), and with the last (4th by default) line being blank, like the following, for instance:
```text
welcome to my gemini blog!!!

check out my latest posts:

```

The SERVER, WHERE_TO, and LINE variables can also all be specified by command line options.


## Usage

```bash
gemp [options] file tag
```

where 'file' is the filename of the markdown file you want to transmit and 'tag' is the tag you want to file it under.

The -u flag will upload the file without adding it to the index of the directory you're publishing it in, allowing you to update a file that is already in place without the index getting clogged up with another entry.

-s allows the server to be specified, although you can also just change the default by editing the file directly

-w allows the directory on the server to be specified, although once again you can edit the default

-l allows the line in the index to which the index entry should be added to be specified, but this can again be edited

Finally, -h prints a help text that explains all of this.


## A brief description of the whole 'tag' thing

If you're into gopher stuff you're probably confused about the tag thing. Basically, the root blog folder is divided into subdirectories that roughly correspond to tags in like a blogspot or wordpress or whatever. These are really only relevant insofar as they're listed next to the name of the post in the index. Plus, you can browse directly to the directory for a tag and list all of the files filed under a particular tag.

If you don't want tags listed next to post titles in the index, you can just remove the "[$2]" part from the part at the end where the post is indexed (should be one of the last few lines, it's the third to last right now but I'm not editing this when I change it next).

If you want to ditch the tag thing altogether, I guess you could remove that, then remove the part where $WHERE_TO/$2 gets made, like that whole if block. Then get rid of $2/ from the scp line where the post is actually sent.


## Misc. notes

This is really bad and I know that but I don't care. I figured maybe someone would find it useful so here it is. I haven't tested this outside my own setup at all and it's entirely possible that there will be little problems that will require you to edit it slightly. idk.
